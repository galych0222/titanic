import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()

    df['Title'] = df['Name'].str.extract(r', (.*?)\.')

    result = []

    for title in ["Mr.", "Mrs.", "Miss."]:
        title_mask = (df['Title'] == title[:-1])

        missing_values = df[title_mask]['Age'].isnull().sum()

        median_age = df[title_mask]['Age'].median()
        median_age = round(median_age) if not pd.isnull(median_age) else median_age

        result.append((title, missing_values, median_age))

    return result
